Readme
Star Wars Skin For phpBannerExchange2.0
Created By James Everett
http://www.geocities.com/jamesehomepage/

<-Instructions->
1 - Upload the file starwars.css into your /bannerexchange/template/css/
2 - If you do not have an /images/ folder in your banner exchange directory,
     create it in the /template/ folder, then upload starwarsbackground.JPG
     into this folder
3 - Go to the admin control panel, click Edit Style Sheet, then use the drop
     down box to select the style sheet to use with your exchange, which should be
     starwars.css
<-End Instructions-> 